#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import copy
import operator
import sys
import csv

# Defining class
class Task:
    def __init__(self, number, execution, deadline, period, window = 0):
        super().__init__()
        self.number = number
        self.execution = execution
        self.deadline = deadline
        self.period = period
        self.window = window

    def __str__(self):
        return "[%s, %s, %s, %s]" % (self.number, self.execution, self.deadline, self.period)

    def __repr__(self):
        return "[%s, %s, %s, %s]" % (self.number, self.execution, self.deadline, self.period)

    def test(self):
        print("Task number: {}".format(self.number))
        print("Execution/processing time: {}".format(self.execution))
        print("Deadline: {}".format(self.deadline))
        print("Period: {}".format(self.period))
        print("Execution window: {}".format(self.window))

# Variables
tasks_count = 0
tasks = []
tasks_periods = []
utilization = 0

# Creating output
output = open('EDF_output.txt', 'w')

# Loading csv file
file = open(sys.argv[1], 'rt')
# file = open('EDF_test1.csv', 'rt')
reader = csv.reader(file)

for index, row in enumerate(reader):    
    # Skipping CSV header row
    if index > 0:
        # Debug
        # print(index,row)
        
        # Setting number of tasks
        tasks_count+=1

        # Creating task and appending to array
        task = Task(int(index),int(row[0]),int(row[1]),int(row[2]))
        tasks.append(task)

file.close()

print("Tasks count:",tasks_count,file=output)
print("Tasks:",tasks,file=output)

# Checking utilization and schedulability
# if feasible, calculate execution
for i in range(tasks_count):
    utilization+=float(tasks[i].execution/tasks[i].period)

print("Utilization:",utilization,file=output)

if utilization > 1:
    print("Schedulable: NO",file=output)
    print("*** Ending execution ***\n",file=output)
else:
    print("Schedulable: YES",file=output)

    # Getting tasks periods
    for i in range(tasks_count):
        tasks_periods.append(tasks[i].period)
    print("Periods: {}".format(tasks_periods),file=output)

    # Finding the LCM
    lcm = np.lcm.reduce(tasks_periods)
    print("LCM: {}".format(lcm),file=output)

    # Creating execution list by task period
    execution = []

    for i in range(tasks_count):
        iteration = 1
        while True:
            if iteration*tasks[i].period <= lcm:
                execution_window = iteration*tasks[i].period

                # using copy to break inheritance
                tasks[i].window = execution_window
                execution.append(copy.copy(tasks[i]))

                if iteration > 1:
                    # -1 gets the last element of the array
                    item = execution[-1]
                    item.deadline = item.deadline + (iteration-1)*(item.period) 

                iteration+=1
            else:
                break

    print("\nInitial execution list: ",file=output)
    print(*execution, sep="\n",file=output)

    # Converting python array to numpy array
    # execution = np.array(execution)

    # Sorting by execution period (inverse) and by deadline
    execution = sorted(execution, key=operator.attrgetter('period'),reverse=True)
    execution = sorted(execution, key=operator.attrgetter('deadline'))

    # Result
    print("\nFinal execution list: ",file=output)
    print(*execution, sep="\n",file=output)
    print("*** Ending execution ***\n",file=output)
    output.close()
