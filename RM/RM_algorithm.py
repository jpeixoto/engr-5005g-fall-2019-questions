#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
import copy
import operator
import sys
import csv

# Defining class
class Task:
    def __init__(self, number, execution, period, start = 0):
        super().__init__()
        self.number = number
        self.execution = execution
        self.period = period
        self.start = start

    def __str__(self):
        # return "[%s, %s, %s, %s]" % (self.number, self.execution, self.period, self.window)
        return "[%s, %s, %s, %s]" % (self.number, self.execution, self.start, self.period)

    def __repr__(self):
        # return "[%s, %s, %s, %s]" % (self.number, self.execution, self.period, self.window)
        return "[%s, %s, %s, %s]" % (self.number, self.execution, self.start, self.period)

    def test(self):
        print("Task number: {}".format(self.number))
        print("Execution/processing time: {}".format(self.execution))
        print("Period: {}".format(self.period))
        print("Execution window: {}".format(self.window))

# Variables
tasks_count = 0
tasks = []
tasks_periods = []
utilization = 0

# Creating output
output = open('RM_output.txt', 'w')

# Loading csv file
file = open(sys.argv[1], 'rt')
# file = open('RM_test1.csv', 'rt')
reader = csv.reader(file)

for index, row in enumerate(reader):    
    # Skipping CSV header row
    if index > 0:
        # Debug
        # print(index,row)
        
        # Setting number of tasks
        tasks_count+=1

        # Creating task and appending to array
        task = Task(int(index),int(row[0]),int(row[1]))
        tasks.append(task)

file.close()

print("Tasks count:",tasks_count,file=output)
print("Tasks:",tasks,file=output)

# Checking utilization and schedulability
# if feasible, calculate execution
for i in range(tasks_count):
    utilization+=float(tasks[i].execution/tasks[i].period)

utilization_bound = tasks_count * (np.power(2,1/tasks_count) - 1)

print("Utilization:",utilization,file=output)
print("Utilization bound (Ub):",utilization_bound,file=output)

if utilization > utilization_bound:
    print("Schedulable: UNCERTAIN",file=output)
    print("*** Ending execution ***\n",file=output)
else:
    print("Schedulable: YES",file=output)

    # Getting tasks periods
    for i in range(tasks_count):
        tasks_periods.append(tasks[i].period)
    print("Periods: {}".format(tasks_periods),file=output)

    # Finding the LCM
    lcm = np.lcm.reduce(tasks_periods)
    print("LCM: {}".format(lcm),file=output)

    # Creating execution list by task period
    execution = []

    for i in range(tasks_count):
        iteration = 1
        while True:
            if iteration*tasks[i].period <= lcm:
                execution_window = iteration*tasks[i].period

                # using copy to break inheritance
                # tasks[i].window = execution_window
                execution.append(copy.copy(tasks[i]))

                if iteration > 1:
                    # -1 gets the last element of the array
                    item = execution[-1]
                    item.start = (iteration-1)*item.period
                    item.period = (iteration)*(item.period) 

                iteration+=1
            else:
                break

    print("\nInitial execution list: ",file=output)
    print(*execution, sep="\n",file=output)

    # Sorting by execution period (inverse) and by deadline
    # execution = sorted(execution, key=operator.attrgetter('period'),reverse=True)
    execution = sorted(execution, key=operator.attrgetter('period'))
    execution = sorted(execution, key=operator.attrgetter('start'))

    # Result
    print("\nSorted execution list: ",file=output)
    print(*execution, sep="\n",file=output)

    # Timeline
    current_time = 0
    total_time = lcm
    priority_list = []

    print("\nTimeline:",file=output)

    while current_time <= total_time:

        # Checking if any task has execution time priority
        # and moving it to the priority array
        for index,item in enumerate(execution):
            if current_time > 0 and item.start == current_time:
                priority_list.append(item)
                execution.pop(index)

        # If the priority array exists, sort by period
        # and add elements back to the top of the execution array
        if len(priority_list) > 0:
            priority_list = sorted(priority_list, key=operator.attrgetter('period'))
            
            # Inverted for loop to make sure that the element with
            # lowest period stays on top of the execution array
            for item in reversed(priority_list):
                execution.insert(0,item)

            # Emptying the priority array
            priority_list = []
            
        # Setting current execution task to the top one of the array
        current_task = execution[0]

        # Checking if any task has to execute at current time
        if current_task.start > current_time:
            print("time {}, no task".format(current_time),file=output)
        else:
            print("time {}, task {}".format(current_time,current_task.number),file=output)
            # Removing one unit from task because it has been executed
            current_task.execution -=1

        # If the task has no more units, remove from list
        if current_task.execution == 0:
            execution.pop(0)

        # Increasing time counter
        current_time+=1

        # Checking if the execution list is empty and ending execution
        if len(execution) == 0:
            print("*** Ending execution ***\n",file=output)
            output.close()
            break
